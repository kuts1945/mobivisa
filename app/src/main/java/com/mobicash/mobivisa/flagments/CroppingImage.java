package com.mobicash.mobivisa.flagments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.isseiaoki.simplecropview.CropImageView;
import com.mobicash.mobivisa.R;
import com.mobicash.mobivisa.RegistrationActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;

import utils.Vars;

/**
 * Created by mobicash on 10/23/15.
 */
public class CroppingImage extends AppCompatActivity {
    Vars vars;
    Bundle extras;
    String path;
    CropImageView cropImageView;
    ImageView croppedImageView;
    FloatingActionButton fab_done;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cropimages);
        fab_done = (FloatingActionButton) findViewById(R.id.fab_tick);
        fab_done.setVisibility(View.GONE);
        cropImageView = (CropImageView)findViewById(R.id.cropImageView);
        croppedImageView = (ImageView)findViewById(R.id.croppedImageView);
        cropImageView.setCropMode(CropImageView.CropMode.RATIO_FREE);
        vars = new Vars(this);
        extras = getIntent().getExtras();
        if(extras!=null){
          path = extras.getString("path");
            vars.log("path======"+path);
            File imgFile = new File(path);

            if(imgFile.exists()){
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                cropImageView.setImageBitmap(myBitmap);
            }
        }


        // Set image for cropping
      //  cropImageView.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.idcard));

        Button cropButton = (Button)findViewById(R.id.crop_button);
        cropButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get cropped image, and show result.
                croppedImageView.setImageBitmap(cropImageView.getCroppedBitmap());
                fab_done.setVisibility(View.VISIBLE);
            }
        });
        fab_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // BitmapDrawable photo = cropImageView.getCroppedBitmap();
                Bitmap image = cropImageView.getCroppedBitmap();

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                Intent viewimage = new Intent(CroppingImage.this, RegistrationActivity.class);
                viewimage.putExtra("image", byteArray);
                startActivity(viewimage);
            }
        });
    }

}
