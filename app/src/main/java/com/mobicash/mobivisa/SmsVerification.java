package com.mobicash.mobivisa;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.util.Random;

import utils.Alerter;
import utils.ConnectionClass;
import utils.Globals;
import utils.Vars;


/**
 * Created by MOBICASH on 26-May-15.
 */
public class SmsVerification extends ActionBarActivity{
    AckReceiver  ackReceiver;
    AlertDialog alertDialog ;
    String msgcode,veri_code;
    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    static Random rnd = new Random();
    Alerter alerter;
    boolean button = false;
    Spinner counrtyCodeSpinner;
    TextView coutrycodeTXT,next_button;
    EditText register_phones,register_code;
    Bundle extr;
    String location;
    String number;
    Vars vars;
    private TextView shinytext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.smslayout);
        shinytext = (TextView) findViewById(R.id.welcome);
        // new Shimmer().start(shinytext);
        extr = getIntent().getExtras();
        vars= new Vars(this);
        if(extr!=null){
            location = extr.getString("location");
        }
        coutrycodeTXT = (TextView) findViewById(R.id.country_code);
        next_button = (TextView) findViewById(R.id.next_button);
        register_phones = (EditText) findViewById(R.id.register_phones);
        register_code= (EditText) findViewById(R.id.register_code);
        //register_code.setVisibility(View.GONE);
        final String countryCode[]= getResources().getStringArray(R.array.option_array_code);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.option_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        counrtyCodeSpinner = (Spinner) findViewById(R.id.sp_countryCode);

        counrtyCodeSpinner.setAdapter(adapter);
        counrtyCodeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                String selectedItem = parent.getItemAtPosition(position).toString();
                Log.i("Log", Integer.toString(position));
                coutrycodeTXT.setText(countryCode[position]);

            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

        ackReceiver = new AckReceiver();
        veri_code = randomString(5);
        alerter =new Alerter(this);
        IntentFilter smsprove = new IntentFilter(Globals.SMSPROVE);
        smsprove.addCategory(Intent.CATEGORY_DEFAULT);

        registerReceiver(ackReceiver, smsprove);
    }
    public void checknumber(View view){
       /* Intent login = new Intent(SmsVerification.this, Full_Registration.class);
        login.putExtra("number", number);
        login.putExtra("location", location);
        login.putExtra("code", coutrycodeTXT.getText());
        startActivity(login);*/
        if(register_phones.getText().toString().equalsIgnoreCase("")){
            Toast.makeText(this,"Please enter phone number",Toast.LENGTH_SHORT).show();
        }else {
            if(button){
                if(register_code.getVisibility()==View.VISIBLE && register_code.getText()
                        .toString().equalsIgnoreCase(veri_code)){
                    //alerter.alerterSuccessSimple("Error", "goood go");
                    Intent login = new Intent(this, RegistrationActivity.class);
                    login.putExtra("number", number);
                    login.putExtra("location", location);
                    login.putExtra("code", coutrycodeTXT.getText());
                    startActivity(login);
                    finish();

                }else{
                    next_button.setText("Resend");
                    button=false;
                    alerter.alerterSuccessSimple("Error", "Entered code don't match");
                }

            }else {

                //getNetworkCountryIso
                String CountryID =GetCountryZipCode(coutrycodeTXT.getText().toString());
                vars.log("CountryID+++++++++++"+CountryID);
                number = coutrycodeTXT.getText() + register_phones.getText().toString();
                PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                try {
                    Phonenumber.PhoneNumber phone_number = phoneUtil.parse(number, CountryID);

                    boolean isValid = phoneUtil.isValidNumber(phone_number);
                    if (isValid) {
                        alertDialog = ProgressDialog.show(this, "", "Please wait...");
                        AQuery aq;
                        aq = new AQuery(this);
                        String url;
                        if (coutrycodeTXT.getText().toString().equalsIgnoreCase("27")) {
                            // url = "http://121.241.242.114:8080/sendsms?username=mca-mobisquid&password=mobisqui&type=0&dlr=1&destination=" + number + "&source=mobicash&message=" + veri_code;

                            url = "http://54.187.96.135:13013/cgi-bin/sendsms?user=Mobicash&pass=Mobicash&smsc=panaceamobile&&text=" + veri_code + "&to=" + number + "&from=43066";
                        } else{
                            url = "http://121.241.242.114:8080/sendsms?username=mca-mobisquid&password=mobisqui&type=0&dlr=1&destination=" + number + "&source=mobicash&message=" + veri_code;
                        }
/*

                    ConnectionClass.returnString(this, url, "SMS", new ConnectionClass.VolleyCallback() {
                        @Override
                        public void onSuccess(String result) {
                            vars.log("After sms======="+result);
                            register_code.setVisibility(View.VISIBLE);
                            next_button.setText("Next");
                            button =true;
                            alertDialog.dismiss();
                        }
                    });
*/

                        // String url = "http://121.241.242.114:8080/sendsms?username=mca-Uganda&password=Uganda&type=0&dlr=1&destination=" + number + "&source=mobicash&message=" + veri_code;
                        aq.ajax(url, String.class, new AjaxCallback<String>() {
                            public void callback(String url, String string,
                                                 AjaxStatus status) {
                                if (status.getCode() == 200 && !string.equals("")) {
                                   // register_code.setVisibility(View.VISIBLE);
                                    next_button.setText("Next");
                                    button =true;
                                    alertDialog.dismiss();
                                    // alerter.alerterSuccessSimple("Error", "Done");
                                } else {
                                    vars.log("status.getCode() ====="+status.getCode());
                                    alertDialog.dismiss();
                                    //   Toast.makeText(SmsVerification.this,"Connection lost",Toast.LENGTH_SHORT).show();
                                    // alerter.alerterSuccessSimple("Error", "connection failed");
                                }
                            }

                        });
                    } else {

                        alerter.alerterSuccessSimple("Error", "Invalid number provided");
                    }
                } catch (NumberParseException e) {
                    System.err.println("NumberParseException was thrown: " + e.toString());
                }

            }
        }
    }
    public class AckReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extras = intent.getExtras();
            if(extras!=null){
                msgcode = extras.getString("msgcode");
                if(veri_code.equalsIgnoreCase(msgcode)) {
                    alertDialog.dismiss();
                    register_code.setText(msgcode);
                    Intent login = new Intent(SmsVerification.this, RegistrationActivity.class);
                    login.putExtra("number", number);
                    login.putExtra("location", location);
                    login.putExtra("code", coutrycodeTXT.getText());
                    startActivity(login);
                    finish();
                    //Toast.makeText(SmsVerification.this, msgcode, Toast.LENGTH_SHORT).show();
                }

            }
            if (intent.getAction() != null) {
                //  Toast.makeText(SmsVerification.this,Globals.SMSPROVE,Toast.LENGTH_SHORT ).show();

            }
        }
    }

    String randomString( int len )
    {
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString();
    }
    public String GetCountryZipCode(String code){
        String CountryID="";
        String CountryZipCode="";

        CountryZipCode= code;
        String[] rl=this.getResources().getStringArray(R.array.CountryCodes);
        for(int i=0;i<rl.length;i++){
            String[] g=rl[i].split(",");
            if(g[0].trim().equals(CountryZipCode.trim())){
                CountryID=g[1];
                break;
            }
        }
        return CountryID;
    }


}