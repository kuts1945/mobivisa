package com.mobicash.mobivisa;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.mobicash.mobivisa.flagments.DatePickerFragment;

import fr.ganfra.materialspinner.MaterialSpinner;
import utils.Vars;

public class VisaApplication extends AppCompatActivity implements View.OnClickListener {
    NetworkImageView profileimage;
    Vars vars;
    ImageLoader imageLoader;
    DialogFragment newFragment;
    View views;
    EditText register_firstName, register_secondname, register_username;
    EditText register_dob, register_palceofbirth, register_countryofbirth, register_nationalaIID, register_visibleidmark, register_currentnationality;
    EditText register_nationalitybirth, register_othernationality, register_presentaddress;
    EditText register_permanetaddress, register_mobile, register_email, register_passport, register_pass_dateofissue;
    EditText register_pass_placeofissue, register_pass_dateofexpiry, register_typeofvisa, register_visanumberofentries;
    EditText register_visa_period, register_visa_expecteddateoftravel, register_PortOfArrival, register_visa_PortOfexit;
    EditText register_Dateofdeparture, register_flight_Datearrival, register_flight_Airline, register_flight_Cityofdeparture;
    EditText register_flightno, register_flight_Cityoftransit, register_flight_ticket;
    MaterialSpinner register_marital, register_occupation, register_ReasonforVisit, register_sex;
    private String[] genders = {"Select", "Female", "Male"};
    private String[] maritalstatus = {"Select", "Divorced", "Single", "Married"};
    private String[] occupation_ = {"Select", "Professional", "Artist", "Military / Police", "Civil Service", "Informal", "Salaried", "Self-employed", "Student", "Others"};
    private String[] reason = {"Select", "Resident", "Immigrant", "Study", "Business", "Transit", "Holiday", "Diplomat", "Employment", "Crew"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visa_application);
        vars = new Vars(this);
        imageLoader = AppController.getInstance(this).getImageLoader();
        profileimage = (NetworkImageView) findViewById(R.id.profileimage);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        register_sex = (MaterialSpinner) findViewById(R.id.register_sex);
        register_marital = (MaterialSpinner) findViewById(R.id.register_marital);
        register_occupation = (MaterialSpinner) findViewById(R.id.register_occupation);
        register_ReasonforVisit = (MaterialSpinner) findViewById(R.id.register_ReasonforVisit);

        register_flight_ticket = (EditText) findViewById(R.id.register_flight_ticket);

        ArrayAdapter<String> genders_ = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, genders);
        ArrayAdapter<String> marital = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, maritalstatus);
        ArrayAdapter<String> occupation = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, occupation_);

        register_sex.setAdapter(genders_);
        register_occupation.setAdapter(occupation);
        register_marital.setAdapter(marital);

        register_dob = (EditText) findViewById(R.id.register_dob);
        register_Dateofdeparture = (EditText) findViewById(R.id.register_Dateofdeparture);
        register_pass_dateofexpiry = (EditText) findViewById(R.id.register_pass_dateofexpiry);
        register_flight_Datearrival = (EditText) findViewById(R.id.register_flight_Datearrival);
        register_palceofbirth = (EditText) findViewById(R.id.register_palceofbirth);
        register_pass_dateofissue = (EditText) findViewById(R.id.register_pass_dateofissue);
        register_visa_expecteddateoftravel = (EditText) findViewById(R.id.register_visa_expecteddateoftravel);

        register_dob.setFocusable(false);
        register_Dateofdeparture.setFocusable(false);
        register_pass_dateofexpiry.setFocusable(false);
        register_flight_Datearrival.setFocusable(false);
        register_palceofbirth.setFocusable(false);
        register_pass_dateofissue.setFocusable(false);
        register_visa_expecteddateoftravel.setFocusable(false);

        register_dob.setOnClickListener(this);
        register_Dateofdeparture.setOnClickListener(this);
        register_pass_dateofexpiry.setOnClickListener(this);
        register_flight_Datearrival.setOnClickListener(this);
        register_palceofbirth.setOnClickListener(this);
        register_pass_dateofissue.setOnClickListener(this);
        register_visa_expecteddateoftravel.setOnClickListener(this);
/*

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
*/

        profileimage.setImageUrl(vars.profile_url, imageLoader);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.register_visa_expecteddateoftravel:
                newFragment = new DatePickerFragment() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        int monthadd = month + 1;
                        register_visa_expecteddateoftravel.setText(year + "-" + monthadd + "-" + day);
                        super.onDateSet(view, year, month, day);

                    }
                };
                newFragment.show(VisaApplication.this.getSupportFragmentManager(), "datePicker");
                break;
            case R.id.register_dob:
                newFragment = new DatePickerFragment() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        int monthadd = month + 1;
                        register_dob.setText(year + "-" + monthadd + "-" + day);
                        super.onDateSet(view, year, month, day);

                    }
                };
                newFragment.show(VisaApplication.this.getSupportFragmentManager(), "datePicker");
                break;
            case R.id.register_Dateofdeparture:
                newFragment = new DatePickerFragment() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        int monthadd = month + 1;
                        register_Dateofdeparture.setText(year + "-" + monthadd + "-" + day);
                        super.onDateSet(view, year, month, day);

                    }
                };
                newFragment.show(VisaApplication.this.getSupportFragmentManager(), "datePicker");
                break;
            case R.id.register_pass_dateofexpiry:
                newFragment = new DatePickerFragment() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        int monthadd = month + 1;
                        register_pass_dateofexpiry.setText(year + "-" + monthadd + "-" + day);
                        super.onDateSet(view, year, month, day);

                    }
                };
                newFragment.show(VisaApplication.this.getSupportFragmentManager(), "datePicker");
                break;
            case R.id.register_flight_Datearrival:
                newFragment = new DatePickerFragment() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        int monthadd = month + 1;
                        register_flight_Datearrival.setText(year + "-" + monthadd + "-" + day);
                        super.onDateSet(view, year, month, day);

                    }
                };
                newFragment.show(VisaApplication.this.getSupportFragmentManager(), "datePicker");
                break;
            case R.id.register_palceofbirth:
                newFragment = new DatePickerFragment() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        int monthadd = month + 1;
                        register_palceofbirth.setText(year + "-" + monthadd + "-" + day);
                        super.onDateSet(view, year, month, day);

                    }
                };
                newFragment.show(VisaApplication.this.getSupportFragmentManager(), "datePicker");
                break;
            case R.id.register_pass_dateofissue:
                newFragment = new DatePickerFragment() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        int monthadd = month + 1;
                        register_pass_dateofissue.setText(year + "-" + monthadd + "-" + day);
                        super.onDateSet(view, year, month, day);

                    }
                };
                newFragment.show(VisaApplication.this.getSupportFragmentManager(), "datePicker");
                break;
            default:
                break;

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_visa_application, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_done:
                if (register_flight_ticket.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(this, "Please enter all fields", Toast.LENGTH_LONG).show();

                }
               // break;

        }
        return true;
    }
}