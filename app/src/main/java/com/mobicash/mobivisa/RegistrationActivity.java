package com.mobicash.mobivisa;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.isseiaoki.simplecropview.CropImageView;
import com.mobicash.mobivisa.flagments.CroppingImage;

import net.grobas.view.PolygonImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import dbstuff.ServerMsg;
import utils.Alerter;
import utils.ConnectionClass;
import utils.Vars;

public class RegistrationActivity extends AppCompatActivity {
    ScrollView scrollView;
    protected Uri mImageCaptureUri,mUri;
    AlertDialog alertDialog_crop ;
    Bundle extras;
    String mylocation,number,countrycode;

    EditText register_firstName,register_secondname,register_username;
    EditText register_email,register_password,register_confermpass;
    EditText register_phone;
    //EditText register_location;
    EditText register_idnumber;
    String encodedFaceString;
    TelephonyManager tel;
    private PolygonImageView mImageView;
    Alerter alerter;
    Gson gson;
    Vars vars;
    ProgressDialog alertDialog;
    Bitmap photo;
    protected AlertDialog dialog;

    protected static final int PICK_FROM_CAMERA = 1;
    protected static final int PICK_FROM_FILE = 3;
    protected static final int CROP_FROM_CAMERA = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
    //    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    //    setSupportActionBar(toolbar);
        byte[] byteArray;
        extras = getIntent().getExtras();
        tel = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        captureImageInitialization();
        alerter = new Alerter(this);
        gson = new Gson();
        vars = new Vars(this);

        register_secondname =(EditText) findViewById(R.id.register_secondname);

        register_password =(EditText) findViewById(R.id.register_password);
        register_confermpass =(EditText) findViewById(R.id.register_confirm_password);

        register_email = (EditText) findViewById(R.id.register_email);
        register_phone = (EditText) findViewById(R.id.register_phone);
        register_firstName = (EditText) findViewById(R.id.register_firstName);
        register_idnumber = (EditText) findViewById(R.id.register_nationalaIID);
        register_username = (EditText) findViewById(R.id.register_username);
        // register_location = (EditText) findViewById(R.id.register_location);

        mImageView= (PolygonImageView) findViewById(R.id.register_facePic);
        if(extras!=null){
            mylocation = extras.getString("location");
            number = extras.getString("number");
            countrycode =extras.getString("code");
            register_phone.setText(number);
            register_phone.setFocusable(false);

            if(extras.getByteArray("image")!=null){
                byteArray = extras.getByteArray("image");
                photo = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                mImageView.setImageBitmap(photo);

            }

        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (encodedFaceString == null) {// CHECK
                    // LOCATION
                    vars.log("ERROR: NO PROFILE PIC");
                    Snackbar.make(view, "ERROR: Please select profile pic", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }	 else if (register_firstName.getText().toString().length() < 3 ) {//
                    register_firstName.setError("name to small");
                    vars.log("ERROR: USERNAME IS TOO SHORT");
                    Snackbar.make(view, "ERROR: First name too short", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                else if (register_secondname.getText().toString().length() < 3)  {//
                    register_secondname.setError("Name to small");
                    vars.log("ERROR: USERNAME IS TOO SHORT");
                    Snackbar.make(view, "ERROR:  Second name too short", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }

                else if (register_username.getText().toString().length() < 3) {//
                    register_username.setError("Too shot");
                    vars.log("ERROR: Invalid ID/Passport number");
                    Snackbar.make(view, "ERROR: Username too shot", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    register_username.requestFocus();
                }
                else if (register_idnumber.getText().toString().length() < 3) {//
                    register_idnumber.setError("Invalid");
                    vars.log("ERROR: Invalid ID/Passport number");
                    Snackbar.make(view, "ERROR: Provide a valid passport number", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                    register_idnumber.requestFocus();
                }else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(register_email.getText().toString()).matches()) {
                    register_email.setError("Invalid email");
                    register_email.requestFocus();
                    vars.log("ERROR: EMAIL IS TOO SHORT");
                    Snackbar.make(view, "ERROR: Email is Invalid", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                }else if (register_password.getText().toString().equalsIgnoreCase("")||register_confermpass.getText().toString().equalsIgnoreCase("")) {
                    register_password.setError("Not accepted");
                    register_confermpass.setError("Not accepted");
                    vars.log("ERROR: EMAIL IS TOO SHORT");
                    Snackbar.make(view, "ERROR: password should not be empty", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                }else if (!register_password.getText().toString().equals(register_confermpass.getText().toString())) {
                    register_password.setError("Not matching");
                    register_confermpass.setError("Not matching");

                    Snackbar.make(view, "ERROR: passwords don't match", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                }

                else{
                    vars.log("==========all is well========");
                    alertDialog = ProgressDialog.show(RegistrationActivity.this, "Registration in progress", "Please wait...");

                    String urlpath = vars.server + "registerusers.php";

                    String[] parameters ={"username","email","mobile","language",
                            "location","code","fullname","password","image"};
                    String[] values ={register_username.getText().toString(),register_email.getText().toString(),
                            register_phone.getText().toString(),"ENGLISH","no","250",register_firstName.getText().toString()+" "+
                            register_secondname.getText().toString(),register_password.getText().toString(),encodedFaceString};


                    ConnectionClass.ConnectionClass(RegistrationActivity.this, urlpath, parameters, values, "FirstActivity", new ConnectionClass.VolleyCallback() {
                        @Override
                        public void onSuccess(String result) {
                            vars.log("result========"+result);
                            ServerMsg gsonMessages = gson.fromJson(result, ServerMsg.class);
                            if (!gsonMessages.getEmail().equalsIgnoreCase("failed")) {


                                   /* gsonMessages.setId(vars.prefs.getString("userId", ""));
                                    gsonMessages.setUsername(vars.prefs.getString("username", ""));
                                    gsonMessages.setUsername(vars.prefs.getString("language", ""));
                                    gsonMessages.setUsername(vars.prefs.getString("location", ""));
                                    gsonMessages.setUsername(vars.prefs.getString("mobile", ""));
                                    gsonMessages.setUsername(vars.prefs.getString("country_code", ""));
                                    gsonMessages.setUsername(vars.prefs.getString("imei", ""));
                                    gsonMessages.setUsername(vars.prefs.getString("fullname", ""));
                                    gsonMessages.setUsername(vars.prefs.getString("profile_url", ""));*/

                                String n = gsonMessages.getId();
                                String usern = gsonMessages.getUsername();
                                String language = gsonMessages.getLanguage();
                                String location = gsonMessages.getLocation();
                                String country = gsonMessages.getCode();
                                String fullname = gsonMessages.getFullname();
                                //String country = mobile.substring(0, Math.min(mobile.length(), 3));
                                String newmobile = register_phone.getText().toString().substring(3);
                                newmobile = "0" + newmobile;

                                vars.edit.putString("userId", n);
                                vars.edit.putString("username", usern);
                                vars.edit.putString("language", language);
                                vars.edit.putString("imei", tel.getDeviceId().toString());
                                vars.edit.putString("mobile", newmobile);
                                vars.edit.putString("country_code", "250");
                                vars.edit.putString("location", location);
                                vars.edit.putString("fullname", fullname);
                                vars.edit.putString("profile_url", gsonMessages.getPicname());
                                vars.log("profile_===" + gsonMessages.getPicname());
                                vars.edit.commit();
                                alertDialog.dismiss();
                                vars.log("====calling success now====my device=="+tel.getDeviceId());
                                alerterSuccessSimple("Success", "Thank you for registering, Enjoy MobiVisa services");

                            } else {
                                //donefinacial = true;
                                alertDialog.dismiss();
                                alerter.alerterSuccessSimple("Error", gsonMessages.getLocation());

                            }

                        }
                    });
                }


            }
        });
    }

    public void selectpic(View veiw){
        if (encodedFaceString==null){
            dialog.show();


        }else{
           /* BitmapDrawable photo = (BitmapDrawable)mImageView.getDrawable();
            Bitmap image = photo.getBitmap();

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            Intent viewimage = new Intent(this, ViewImageFull.class);
            viewimage.putExtra("image", byteArray);
            startActivity(viewimage);*/
        }
    }
    private void captureImageInitialization() {

        final String[] items = new String[] { "Take from camera",
                "Select from gallery" };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.select_dialog_item, items);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) { // pick from
                // camera
                if (item == 0) {

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    mImageCaptureUri = Uri.fromFile(new File(Environment
                            .getExternalStorageDirectory(), "tmp_avatar_"
                            + String.valueOf(System.currentTimeMillis())
                            + ".png"));


                    intent.putExtra(MediaStore.EXTRA_OUTPUT,
                            mImageCaptureUri);

                    try {
                        intent.putExtra("return-data", true);

                        startActivityForResult(intent, PICK_FROM_CAMERA);
                    } catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    }
                } else {
                    // pick from file
                    /**
                     * To select an image from existing files, use
                     * Intent.createChooser to open image chooser. Android will
                     * automatically display a list of supported applications,
                     * such as image gallery or file manager.
                     */

                    startActivityForResult(
                            Intent.createChooser(
                                    new Intent(Intent.ACTION_GET_CONTENT)
                                            .setType("image/*"), "Choose an image"),
                            PICK_FROM_FILE);

                }
            }
        });

        dialog = builder.create();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK)
            return;

        switch (requestCode) {

            case PICK_FROM_CAMERA:
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this
                            .getContentResolver(), mImageCaptureUri);
                    cropimage(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                break;

            case PICK_FROM_FILE:
                /**
                 * After selecting image from files, save the selected path
                 */
                mImageCaptureUri = data.getData();

                try{
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this
                            .getContentResolver(), mImageCaptureUri);

                    cropimage(bitmap);

                    String  mTmpGalleryPicturePath = getPath(mImageCaptureUri);

                 //   TextView messageTxt = (TextView) promptsView.findViewById(R.id.success_simple_message);



                   /* Intent crop = new Intent(this, CroppingImage.class);

                    crop.putExtra("path", mTmpGalleryPicturePath);
                    vars.log("calling cropping=======");
                    startActivity(crop);*/

                //    File files = saveImage(bitmap, "delete", this);

             //       doCrop(Uri.fromFile(files));
                    //files.delete();
                }catch(ActivityNotFoundException | IOException ex){
                    vars.log("asasasasasasas");
                    String errorMessage = "Sorry - your device doesn't support the crop action!";
                    Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
                    toast.show();
                }



                break;


            case CROP_FROM_CAMERA:
                Bundle extras = data.getExtras();
                vars.log("CROP_FROM_CAMERA====");
                /**
                 * After cropping the image, get the bitmap of the cropped image and
                 * display it on imageview.
                 */
                if (extras != null) {

                    Bitmap photo = extras.getParcelable("data");

                    mImageView.setImageBitmap(photo);

                    mImageCaptureUri = data.getData();

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    photo.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    //    files_profile = saveImage(photo, "users", context);
                    byte[] imageBytes = baos.toByteArray();
                    encodedFaceString = Base64.encodeToString(imageBytes,
                            Base64.DEFAULT);
                    vars.log("====not null======encodedFaceString============");


                }

                break;


        }
    }

    protected void doCrop(Uri picUri) {
        //call the standard crop action intent
        Intent cropIntent = new Intent("com.android.camera.action.CROP");
        //indicate image type and Uri of image
        cropIntent.setDataAndType(picUri, "image/*");
        //set crop properties
        cropIntent.putExtra("crop", "true");
        //indicate aspect of desired crop
        cropIntent.putExtra("aspectX", 1);
        cropIntent.putExtra("aspectY", 1);
        //indicate output X and Y
        cropIntent.putExtra("outputX", 256);
        cropIntent.putExtra("outputY", 256);
        //retrieve data on return
        cropIntent.putExtra("return-data", true);
        //start the activity - we handle returning in onActivityResult
        //filedel = true;
        vars.log("docrop====");
        startActivityForResult(cropIntent, CROP_FROM_CAMERA);
    }
    public File saveImage(Bitmap thePic, String fileName, Context context) {
        OutputStream fOut = null;


        File m = new File(Environment.getExternalStorageDirectory(), "/SugaWorld/media");
        if (!m.exists()) {
            m.mkdirs();
        }


        if (!m.exists()) {
            m.mkdirs();
        }
        String strDirectory = m.toString();
        File f = new File(m, fileName);
        vars.log("output directory:" + strDirectory);
        try {
            fOut = new FileOutputStream(f);

            /** Compress image **/
            thePic.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
            fOut.flush();
            fOut.close();

            /** Update image to gallery **/
            MediaStore.Images.Media.insertImage(context.getContentResolver(),
                    f.getAbsolutePath(), f.getName(), f.getName());
            vars.log("IMAGE SAVED........");



        } catch (Exception e) {
            e.printStackTrace();
        }
        return f;
    }
    public void alerterSuccessSimple(String header, String message) {

        final String header2 = header;

        LayoutInflater li = LayoutInflater.from(this);
        View promptsView;
        promptsView = li.inflate(R.layout.dialog_success_simple, null);

        TextView headerTxt = (TextView) promptsView.findViewById(R.id.success_simple_header);
        TextView messageTxt = (TextView) promptsView.findViewById(R.id.success_simple_message);

        headerTxt.setText(header);
        messageTxt.setText(message);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        // set dialog message
        alertDialogBuilder
                .setCancelable(false)

                .setNegativeButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent mainpage = new Intent(RegistrationActivity.this, MainActivity.class);
                                startActivity(mainpage);
                                finish();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    @SuppressLint("NewApi")
    private String getPath(Uri uri) {
        if( uri == null ) {
            return null;
        }

        String[] projection = { MediaStore.Images.Media.DATA };

        Cursor cursor;
        if(Build.VERSION.SDK_INT >19)
        {
            // Will return "image:x*"
            String wholeID = DocumentsContract.getDocumentId(uri);
            // Split at colon, use second item in the array
            String id = wholeID.split(":")[1];
            // where id is equal to
            String sel = MediaStore.Images.Media._ID + "=?";

            cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    projection, sel, new String[]{ id }, null);
        }
        else
        {
            cursor = getContentResolver().query(uri, projection, null, null, null);
        }
        String path = null;
        try
        {
            int column_index = cursor
                    .getColumnIndex(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            path = cursor.getString(column_index).toString();
            cursor.close();
        }
        catch(NullPointerException e) {

        }
        return path;
    }
    public void cropimage (final Bitmap bitmap){
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView;
        promptsView = li.inflate(R.layout.cropping, null);

        final CropImageView cropImageView = (CropImageView) promptsView.findViewById(R.id.cropImageView);
        cropImageView.setCropMode(CropImageView.CropMode.RATIO_FREE);
        cropImageView.setImageBitmap(bitmap);

        FloatingActionButton fab_done = (FloatingActionButton) promptsView.findViewById(R.id.fab_tick);
        FloatingActionButton fab_delete = (FloatingActionButton) promptsView.findViewById(R.id.fab_delte);
        fab_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mImageView.setImageBitmap(cropImageView.getCroppedBitmap());

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                //    files_profile = saveImage(photo, "users", context);
                byte[] imageBytes = baos.toByteArray();
                encodedFaceString = Base64.encodeToString(imageBytes,
                        Base64.DEFAULT);

                alertDialog_crop.dismiss();
            }
        });
        fab_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog_crop.dismiss();
            }
        });

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);
        alertDialog_crop = alertDialogBuilder.create();
        alertDialog_crop.show();

    }
}
