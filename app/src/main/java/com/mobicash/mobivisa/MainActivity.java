package com.mobicash.mobivisa;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.toolbox.ImageRequest;

import com.mobicash.mobivisa.flagments.Home;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionButton;
import com.oguzdev.circularfloatingactionmenu.library.SubActionButton;

import net.grobas.view.PolygonImageView;

import utils.Vars;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener,Home.OnFragmentInteractionListener {
    Vars vars;
     static final String  CHAT ="chat";
     static final String MOBICASH ="mobicash";
     static final String APPLY_VISA ="applyvisa";
     static final String TICKET ="ticket";
    PolygonImageView profile_img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        vars = new Vars(this);
        if(vars.chk==null){
            vars.log("=====Firsttime====");
            Intent first = new Intent(this,IndexPage.class);
            startActivity(first);
            finish();

        }
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        Home llf = new Home();
        ft.replace(R.id.main_content, llf);
        ft.commit();


        final ImageView fabIconNew = new ImageView(this);
        fabIconNew.setImageResource(R.drawable.ic_action_new_light);

       FloatingActionButton rightLowerButton = new FloatingActionButton.Builder(this)
               .setContentView(fabIconNew)
               .setBackgroundDrawable(R.drawable.button_selector)
               .build();

        ImageView chat_con1 = new ImageView(this);
        ImageView apply_con2 = new ImageView(this);
        ImageView ticket_rlIcon3 = new ImageView(this);
        ImageView mobi_con4 = new ImageView(this);


        chat_con1.setImageResource(R.drawable.chat);
        apply_con2.setImageResource(R.drawable.visa);
        ticket_rlIcon3.setImageResource(R.drawable.ticket);
        mobi_con4.setImageResource(R.mipmap.ic_launcher_icon);



        SubActionButton.Builder itembuilder = new SubActionButton.Builder(this);
        itembuilder.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_selector_small));

        SubActionButton chatbt = itembuilder.setContentView(chat_con1).build();
        SubActionButton apply = itembuilder.setContentView(apply_con2).build();
        SubActionButton mobicash = itembuilder.setContentView(mobi_con4).build();
        SubActionButton tickets = itembuilder.setContentView(ticket_rlIcon3).build();

        apply.setTag(APPLY_VISA);
        chatbt.setTag(CHAT);
        mobicash.setTag(MOBICASH);
        tickets.setTag(TICKET);

        chatbt.setOnClickListener(this);
        apply.setOnClickListener(this);
        tickets.setOnClickListener(this);
        mobicash.setOnClickListener(this);


        FloatingActionMenu rightLowerMenu = new FloatingActionMenu.Builder(this)
                .addSubActionView(chatbt)
                .addSubActionView(apply)
                .addSubActionView(mobicash)
                .addSubActionView(tickets)
                .attachTo(rightLowerButton)
                .build();


        rightLowerMenu.setStateChangeListener(new FloatingActionMenu.MenuStateChangeListener() {
            @Override
            public void onMenuOpened(FloatingActionMenu menu) {
                // Rotate the icon of rightLowerButton 45 degrees clockwise
                fabIconNew.setRotation(0);
                PropertyValuesHolder pvhR = PropertyValuesHolder.ofFloat(View.ROTATION, 45);
                ObjectAnimator animation = ObjectAnimator.ofPropertyValuesHolder(fabIconNew, pvhR);
                animation.start();
            }

            @Override
            public void onMenuClosed(FloatingActionMenu menu) {
                // Rotate the icon of rightLowerButton 45 degrees counter-clockwise
                fabIconNew.setRotation(45);
                PropertyValuesHolder pvhR = PropertyValuesHolder.ofFloat(View.ROTATION, 0);
                ObjectAnimator animation = ObjectAnimator.ofPropertyValuesHolder(fabIconNew, pvhR);
                animation.start();
            }
        });




        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        LayoutInflater li = LayoutInflater.from(this);
        View promptsView;
        promptsView = li.inflate(R.layout.nav_header_main, null);

        TextView users_name = (TextView) promptsView.findViewById(R.id.users_name);
        TextView number = (TextView) promptsView.findViewById(R.id.tv_mobile);
        profile_img = (PolygonImageView) promptsView.findViewById(R.id.profile_image);
        users_name.setText(vars.fullname);
        number.setText(vars.mobile);
        vars.log("Url ====" + vars.profile_url);


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.addHeaderView(promptsView);

        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onResume() {
        ImageRequest load= new ImageRequest(vars.profile_url, new Response.Listener<Bitmap>() {

            @Override
            public void onResponse(Bitmap response) {
                profile_img.setImageBitmap(response);

            }
        }, 0, 0, null, null);
        AppController.getInstance(this).addToRequestQueue(load);
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            vars.edit.clear();
            vars.edit.commit();
            Intent goton = new Intent(this,IndexPage.class);
            startActivity(goton);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_visaapplication) {
         Intent app = new Intent(this,VisaApplication.class);
            startActivity(app);
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_chat) {

        } else if (id == R.id.nav_settings) {
            Intent setting = new Intent(this,SettingsActivity.class);
            startActivity(setting);


        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        if(v.getTag().equals(CHAT)){
            Toast.makeText(this,"chat",Toast.LENGTH_LONG).show();
        }
        if(v.getTag().equals(APPLY_VISA)){
            Intent visa = new Intent(this,VisaApplication.class);
            startActivity(visa);
        }
        if(v.getTag().equals(MOBICASH)){

        }
        if(v.getTag().equals(TICKET)){
            vars.log("=============ticket============");

        }

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
