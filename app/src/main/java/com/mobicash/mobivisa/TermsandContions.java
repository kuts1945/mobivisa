package com.mobicash.mobivisa;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;




public class TermsandContions extends AppCompatActivity {
    Button nxt;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.terms_and_conditions);
        nxt = (Button) findViewById(R.id.nxtbut);
        nxt.setVisibility(View.GONE);
        TextView foo = (TextView)findViewById(R.id.read);
        foo.setText(Html.fromHtml(getString(R.string.nice_html)));

    }
    public void nextbut(View v){
        Intent next = new Intent(this, SmsVerification.class);
        startActivity(next);
        finish();

    }
    public void onCheckboxClicked(View v){
        boolean checked = ((CheckBox) v).isChecked();

        switch (v.getId()){
            case R.id.checkBox1:
                if(checked){
                    nxt.setVisibility(View.VISIBLE);
                }
                else{
                    nxt.setVisibility(View.GONE);
                }
        }


    }




}
