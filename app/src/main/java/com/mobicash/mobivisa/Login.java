package com.mobicash.mobivisa;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;


import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import dbstuff.ServerMsg;
import dbstuff.TransactionObj;
import utils.Alerter;
import utils.ConnectionClass;
import utils.Globals;
import utils.Vars;


public class Login extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {
	String resultimage;
	//String resultimage;

	EditText tv_username, password;
	TextView tv_forgtpwd;
	TelephonyManager tel;
	private ProgressDialog mDialog;
	RadioButton radio_number,radio_id,radio_username;
	Vars vars;
	Alerter alerter;
	ProgressDialog dialog;
	Gson gson;
	RelativeLayout bottomPanel;
	Bitmap photo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//   this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView( R.layout.activity_login_cyclos);
		bottomPanel = (RelativeLayout) findViewById(R.id.bottomPanel);
		vars = new Vars(this);
		tel = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		alerter = new Alerter(this);
		radio_number = (RadioButton) findViewById(R.id.radio_number);
		radio_number.setChecked(true);

		//radio_id = (RadioButton) findViewById(R.id.radio_id);
		radio_username = (RadioButton) findViewById(R.id.radio_username);
		radio_number.setOnCheckedChangeListener(this);
		//radio_id.setOnCheckedChangeListener(this);
		radio_username.setOnCheckedChangeListener(this);
		gson = new Gson();
		//setTitle("Login");
		Button login = (Button) findViewById(R.id.btn_login);
		tv_username = (EditText) findViewById(R.id.et_username);
		password = (EditText) findViewById(R.id.ev_password);
		//tv_forgtpwd = (TextView) findViewById(R.id.tv_forget_pwd);
		bottomPanel.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				vars.hideKeyboard(v,Login.this);
				return false;
			}
		});
		if(radio_number.isChecked()){
			tv_username.setHint("Mobile");
		//	radio_id.setChecked(false);
			tv_username.setInputType(InputType.TYPE_CLASS_PHONE);
			radio_username.setChecked(false);
		}

	}
	public void login_now(View v){
		if(tv_username.getText().toString().equalsIgnoreCase("")|| password.getText().toString().equalsIgnoreCase("")) {
			Toast.makeText(this, "Enter all fields", Toast.LENGTH_LONG).show();
		}
		else if(radio_number.isChecked() && tv_username.getText().toString().length()<10){
			Toast.makeText(this,"Please enter a valid number",Toast.LENGTH_LONG).show();
		}
		else {
				if (radio_number.isChecked()) {
			dialog = ProgressDialog.show(this, "Checking Credentials", "Please wait...");
				/*String url = vars.server_rw + "client_login.php";

				String[] parameter = {"clientPin", "clientNumber", "imei"};
				String[] values = {password.getText().toString().trim(), tv_username.getText().toString().trim(), tel.getDeviceId()};

				ConnectionClass.ConnectionClass(this, url, parameter, values, "Loginn", new ConnectionClass.VolleyCallback() {
					@Override
					public void onSuccess(String result) {
						final TransactionObj gsonMessage = new Gson().fromJson(result, TransactionObj.class);
						if (gsonMessage.getResult().equalsIgnoreCase("Success")) {
*/

			String[] paramenters = {"username", "password"};
			String[] values = {tv_username.getText().toString().trim(), password.getText().toString().trim()};
			String urlpath = vars.server + "logincheck.php";

			ConnectionClass.ConnectionClass(Login.this, urlpath, paramenters, values, "checkmobisquid", new ConnectionClass.VolleyCallback() {
				@Override
				public void onSuccess(String serverstring) {
					vars.log("from mobisquid===" + serverstring);
					ServerMsg trans = gson.fromJson(serverstring, ServerMsg.class);

					if (trans.getUsername().equalsIgnoreCase(tv_username.getText().toString().trim())) {

						String mobile = trans.getMobile();
						String newmobile = mobile.substring(3);
						newmobile = "0" + newmobile;
						vars.log("full name===" + trans.getFullname());
						vars.log("id=====" + trans.getId());
						vars.edit.putString("userId", trans.getId());
						vars.edit.putString("username", trans.getFullname());
						vars.edit.putString("language", trans.getLanguage());
						vars.edit.putString("imei", tel.getDeviceId().toString());
						vars.edit.putString("mobile", newmobile);
						vars.edit.putString("country_code", "250");
						vars.edit.putString("location", trans.getLocation());
						vars.edit.putString("fullname", trans.getFullname());
						vars.edit.putString("profile_url", trans.getPicname());
						vars.edit.putString("active", "activenow");
						vars.log("profile_url===" + trans.getImagename());
						vars.log("profile_===" + trans.getPicname());
						vars.edit.commit();

						//alerter.alerterSuccessSimple("Error", trans.getMobile());
						dialog.dismiss();
						alerterSuccessSimple("Success", "Enjoy MobiSquid");
						AppController.getInstance(Login.this).cancelPendingRequests("Login");


					} else {

						{
							dialog.dismiss();
							alerter.alerterSuccessSimple("Error", "Wrong username or password");
							//}

							//	}

						}
					}
				}

			});
		}else{
					dialog = ProgressDialog.show(this, "Checking Credentials", "Please wait...");
					String[] paramenters = {"username","password"};
					String[] values = {tv_username.getText().toString().trim(),password.getText().toString().trim()};
					String urlpath = vars.server+"logincheck.php";

					ConnectionClass.ConnectionClass(Login.this, urlpath, paramenters, values, "checkmobisquid",
							new ConnectionClass.VolleyCallback() {
						@Override
						public void onSuccess(String serverstring) {
							vars.log("from mobisquid===" + serverstring);
							ServerMsg trans = gson.fromJson(serverstring, ServerMsg.class);

							if (trans.getUsername().equalsIgnoreCase(tv_username.getText().toString().trim())) {

								String mobile = trans.getMobile();
								String newmobile = mobile.substring(3);
								newmobile = "0" + newmobile;
								vars.log("full name===" + trans.getFullname());
								vars.log("id=====" + trans.getId());
								vars.edit.putString("userId", trans.getId());
								vars.edit.putString("username", trans.getFullname());
								vars.edit.putString("language", trans.getLanguage());
								vars.edit.putString("imei", tel.getDeviceId().toString());
								vars.edit.putString("mobile", newmobile);
								vars.edit.putString("country_code", "250");
								vars.edit.putString("location", trans.getLocation());
								vars.edit.putString("fullname", trans.getFullname());
								vars.edit.putString("profile_url", trans.getPicname());
								vars.edit.putString("active", "activenow");
								vars.log("profile_url===" + trans.getImagename());
								vars.log("profile_===" + trans.getPicname());
								vars.edit.commit();

								//alerter.alerterSuccessSimple("Error", trans.getMobile());
								dialog.dismiss();
								alerterSuccessSimple("Success", "Enjoy MobiVisa");
								AppController.getInstance(Login.this).cancelPendingRequests("Login");
							}else{
								dialog.dismiss();
								alerter.alerterSuccessSimple("Error", "Wrong username or password");
							}
						}

				});
				}
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (isChecked) {
			if (buttonView.getId() == R.id.radio_number) {
				tv_username.setHint("phone number");
				//radio_id.setChecked(false);
				tv_username.setInputType(InputType.TYPE_CLASS_PHONE);
				radio_username.setChecked(false);
			}
		/*	if (buttonView.getId() == R.id.radio_id) {
				radio_number.setChecked(false);
				tv_username.setHint("national ID");
				tv_username.setInputType(InputType.TYPE_CLASS_TEXT);
				radio_username.setChecked(false);
			}*/
			if (buttonView.getId() == R.id.radio_username) {
				radio_number.setChecked(false);
				tv_username.setInputType(InputType.TYPE_CLASS_TEXT);
				tv_username.setHint("username");
				//radio_id.setChecked(false);
			}
		}
	}
	public void register(View view){
		if(vars.active==null && vars.mobile!=null) {
			/*String number = vars.mobile.substring(1);
			Intent login = new Intent(this, Full_Registration.class);
			login.putExtra("number", vars.country_code+number);
			login.putExtra("location", vars.location);
			login.putExtra("code", vars.country_code);
			startActivity(login);
			finish();*/
		}else {
			Intent reg = new Intent(this, MainActivity.class);
			startActivity(reg);
		}
	}
	public void alerterSuccessSimple(String header, String message) {

		final String header2 = header;

		LayoutInflater li = LayoutInflater.from(this);
		View promptsView;
		promptsView = li.inflate(R.layout.dialog_success_simple, null);

		TextView headerTxt = (TextView) promptsView.findViewById(R.id.success_simple_header);
		TextView messageTxt = (TextView) promptsView.findViewById(R.id.success_simple_message);

		headerTxt.setText(header);
		messageTxt.setText(message);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				this);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder
				.setCancelable(false)

				.setNegativeButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								Intent mainpage = new Intent(Login.this, MainActivity.class);
								startActivity(mainpage);
								finish();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_login_cyclos, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		/*if (id == R.id.action_settings) {
			return true;
		}
*/
		return super.onOptionsItemSelected(item);
	}

	public void upload(final String[] parameters, final String[] values,String URL_FEED, final String urlimage,final TransactionObj gsonMessage) {

		StringRequest postRequest = new StringRequest(Request.Method.POST, URL_FEED,
				new Response.Listener<String>() {

					@Override
					public void onResponse(String serverstring) {
						// TODO Auto-generated method stub
						if (serverstring != null) {
							//alertDialog.dismiss();
							vars.log("serverstring==="+serverstring);

								ServerMsg trans = gson.fromJson(serverstring, ServerMsg.class);

								if (!trans.getMobile().equalsIgnoreCase("no details")) {

									trans.setId(vars.prefs.getString("userId", ""));
									trans.setUsername(vars.prefs.getString("username", ""));
									trans.setUsername(vars.prefs.getString("language", ""));
									trans.setUsername(vars.prefs.getString("location", ""));
									trans.setUsername(vars.prefs.getString("mobile", ""));
									trans.setUsername(vars.prefs.getString("country_code", ""));
									trans.setUsername(vars.prefs.getString("imei", ""));
									trans.setUsername(vars.prefs.getString("fullname", ""));
									trans.setUsername(vars.prefs.getString("profile_url", ""));



									String mobile  = trans.getMobile();
									String newmobile = mobile.substring(3);
									newmobile = "0" + newmobile;
									vars.log("!trans.getMobile()");
									vars.edit.putString("userId", trans.getId());
									vars.edit.putString("username", trans.getFullname());
									vars.edit.putString("language", trans.getLanguage());
									vars.edit.putString("imei", tel.getDeviceId().toString());
									vars.edit.putString("mobile", newmobile);
									vars.edit.putString("country_code", "250");
									vars.edit.putString("location", trans.getLocation());
									vars.edit.putString("fullname", trans.getFullname());
									vars.edit.putString("profile_url", trans.getPicname());
									vars.edit.putString("active","activenow");
									vars.log("profile_url===" + trans.getImagename());
									vars.log("profile_==="+trans.getPicname());
									vars.edit.commit();

									//alerter.alerterSuccessSimple("Error", trans.getMobile());
									dialog.dismiss();
									alerterSuccessSimple("Success","Enjoy MobiSquid");
									AppController.getInstance(Login.this).cancelPendingRequests("Login");


								}else{
									vars.log("getMobile()");
									downloadimage(urlimage, new VolleyCallback() {

										@Override
										public void onSuccess(final String stringimage) {
											vars.log("=========time to move on ============================"+stringimage);

											String urlpath = vars.server + "registerusers.php";
											String code = gsonMessage.getClient().substring(0, Math.min(gsonMessage.getClient().length(), 3));
											String[] parameters ={"password","username","email","mobile","language","gender",
													"location","dob","code","fullname","image"};
											String[] values ={password.getText().toString(),gsonMessage.getDetails(),gsonMessage.getExtra1(),
													gsonMessage.getClient().trim(),"ENGLISH",gsonMessage.getTransactionid(),
													"Kigali Rwanda",gsonMessage.getConfNum(),code,gsonMessage.getDetails()
											,stringimage};
											ConnectionClass.ConnectionClass(Login.this, urlpath, parameters, values, "Logintwo", new ConnectionClass.VolleyCallback() {
												@Override
												public void onSuccess(String finslresult) {
													vars.log("============SUCCESS=========");

													final ServerMsg gsonMessages = new Gson().fromJson(finslresult, ServerMsg.class);
													final String mobile;
													mobile = gsonMessage.getClient();
													vars.log("=====No connection to server==" + Globals.CONNECTION);

													if (!gsonMessages.getEmail().equalsIgnoreCase("failed")) {

														if (vars.prefs.contains("userId")) {

															gsonMessages.setId(vars.prefs.getString("userId", ""));
															gsonMessages.setUsername(vars.prefs.getString("username", ""));
															gsonMessages.setUsername(vars.prefs.getString("language", ""));
															gsonMessages.setUsername(vars.prefs.getString("location", ""));
															gsonMessages.setUsername(vars.prefs.getString("mobile", ""));
															gsonMessages.setUsername(vars.prefs.getString("country_code", ""));
															gsonMessages.setUsername(vars.prefs.getString("imei", ""));
															gsonMessages.setUsername(vars.prefs.getString("fullname", ""));
															gsonMessages.setUsername(vars.prefs.getString("profile_url", ""));

														}
														String n = gsonMessages.getId();
														String usern = gsonMessages.getUsername();
														String language = gsonMessages.getLanguage();
														String location = gsonMessages.getLocation();
														String country = gsonMessages.getCode();
														String fullname = gsonMessages.getFullname();
														//String country = mobile.substring(0, Math.min(mobile.length(), 3));
														String newmobile = mobile.substring(3);
														newmobile = "0" + newmobile;
														SharedPreferences.Editor editor = vars.prefs.edit();
														editor.putString("userId", n);
														editor.putString("username", usern);
														editor.putString("language", language);
														editor.putString("imei", tel.getDeviceId().toString());
														editor.putString("mobile", newmobile);
														editor.putString("country_code", "250");
														editor.putString("location", location);
														editor.putString("fullname", fullname);
														editor.putString("profile_url", gsonMessages.getPicname());

														editor.putString("active","activenow");
														vars.log("profile_===" + gsonMessages.getPicname());
														editor.commit();
														dialog.dismiss();
														vars.log("====calling success now====my device=="+tel.getDeviceId());
														alerterSuccessSimple("Success", "Thank you for registering, enjoy MobiSquid services");

													} else {
														//donefinacial = true;
														dialog.dismiss();
														alerter.alerterSuccessSimple("Error", gsonMessages.getMobile());

													}

												}
											});


										}
									});


								}

						}
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						vars.log("Error:===== " + error.getMessage());
						//alertDialog.dismiss();

						//Toast.makeText(getApplicationContext(), "No connection to server", Toast.LENGTH_LONG).show();
						//conn_error.setVisibility(View.VISIBLE);

					}

				}) {
			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();

				for(int y =0;y<parameters.length;y++){
					params.put(parameters[y], values[y]);
					vars.log("para "+y+"==="+parameters[y]+"====and =="+"values "+y+"==="+values[y]);
				}


				return params;
			}
		};
		postRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

		AppController.getInstance(this).addRequest(postRequest, "Login");
	}
	public String downloadimage(String url,final VolleyCallback callback){

		ImageRequest download = new ImageRequest(url, new Response.Listener<Bitmap>() {

			@Override
			public void onResponse(Bitmap response) {
				//iv.setImageBitmap(response);
				photo = response;
				getbitmap(photo, new VolleyCallback() {
					@Override
					public void onSuccess(String result) {
						resultimage = result;
						callback.onSuccess(resultimage);
					}
				});
				//TestTask task = new TestTask(callback);
				//task.execute(photo);
				vars.log("===The image url was found===");


			}

		}, 0, 0, null, new Response.ErrorListener(){

			@Override
			public void onErrorResponse(VolleyError error) {
				Drawable myDrawable = getResources().getDrawable(R.drawable.noimage);
				Bitmap myLogo = ((BitmapDrawable) myDrawable).getBitmap();
				photo = myLogo;


				if(error instanceof TimeoutError || error instanceof NoConnectionError){
					vars.log("error TimeoutError");
					photo = myLogo;
					getbitmap(photo, new VolleyCallback() {
						@Override
						public void onSuccess(String result) {
							resultimage = result;
							callback.onSuccess(resultimage);
						}
					});
					//TestTask task = new TestTask(callback);
					//task.execute(photo);
					//callback.onSuccess(resultimage);
					// AppController.getInstance(context).cancelPendingRequests(tag);
				}
				else if(error instanceof NetworkError){
					vars.log("error NetworkError");
					photo = myLogo;
					getbitmap(photo, new VolleyCallback() {
						@Override
						public void onSuccess(String result) {
							resultimage = result;
							callback.onSuccess(resultimage);
						}
					});
				//	callback.onSuccess(resultimage);
				}
				else if(error instanceof ServerError){
					vars.log("error ServerError");
					photo = myLogo;
					getbitmap(photo, new VolleyCallback() {
						@Override
						public void onSuccess(String result) {
							resultimage = result;
							callback.onSuccess(resultimage);
						}
					});
					//callback.onSuccess(resultimage);
				}
				else if(error instanceof ParseError){
					vars.log("error ParseError");
					photo = myLogo;
					getbitmap(photo, new VolleyCallback() {
						@Override
						public void onSuccess(String result) {
							resultimage = result;
							callback.onSuccess(resultimage);
						}
					});
					//callback.onSuccess(resultimage);
				}
				else if(error instanceof AuthFailureError){
					vars.log("error AuthFailureError");
					photo = myLogo;
					getbitmap(photo, new VolleyCallback() {
						@Override
						public void onSuccess(String result) {
							resultimage = result;
							callback.onSuccess(resultimage);
						}
					});
					//callback.onSuccess(resultimage);
				}

			}
		});

		download.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

		AppController.getInstance(this).addRequest(download, "Login");
		return resultimage;
	}

	public interface VolleyCallback{
		void onSuccess(String result);
	}
/*	private class TestTask extends AsyncTask<Bitmap, Void, String> {
		VolleyCallback handler=null;
		public TestTask(VolleyCallback handler){
			this.handler = handler;
		}
		@Override
		protected String doInBackground(Bitmap... params) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			photo.compress(Bitmap.CompressFormat.JPEG, 100, baos);
			byte[] imageBytes = baos.toByteArray();
			resultimage = Base64.encodeToString(imageBytes,
					Base64.DEFAULT);
			vars.log("=========ENCODEDED STRING IS done========== ");
			return resultimage;
		}

		@Override
		protected void onPostExecute(String result) {
			if (result != null) {
				vars.log("String not null"+result);
				resultimage = result;
				this.handler.onSuccess(resultimage);
			}

		}

	}*/
	public void getbitmap(final Bitmap photo, final VolleyCallback volleyCallback){

		class TestTask extends AsyncTask<Bitmap, Void, String> {


			@Override
			protected String doInBackground(Bitmap... params) {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				photo.compress(Bitmap.CompressFormat.JPEG, 100, baos);
				byte[] imageBytes = baos.toByteArray();
				resultimage = Base64.encodeToString(imageBytes,
						Base64.DEFAULT);
				vars.log("=========ENCODEDED STRING IS done========== ");
				return resultimage;
			}

			@Override
			protected void onPostExecute(String result) {
				if (result != null) {
					vars.log("String not null"+result);
					resultimage = result;
					volleyCallback.onSuccess(resultimage);

				}

			}

		}
		TestTask task =new TestTask();
		task.execute();
	}
}