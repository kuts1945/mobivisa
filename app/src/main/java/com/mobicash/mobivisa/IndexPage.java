package com.mobicash.mobivisa;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.mobicash.mobivisa.flagments.CroppingImage;

import utils.Vars;


public class IndexPage extends AppCompatActivity {
    IndexPageFragment mFirstFragment;
    Vars vars;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index_page);
        vars = new Vars(this);

        mFirstFragment = new IndexPageFragment();
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
      //  fragmentTransaction.add(R.id.fragment_place, mFirstFragment);
        fragmentTransaction.commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void registerclient(View view) {
        Intent register = new Intent(this,SmsVerification.class);
        startActivity(register);
    }

    public void login(View view) {
        Intent regi = new Intent(IndexPage.this,Login.class);
        //Intent regi = new Intent(IndexPage.this,VisaApplication.class);
        startActivity(regi);
       /* if (vars.active==null){
            Snackbar.make(view, "Hello: You need to Upgrade for financial services", Snackbar.LENGTH_LONG)
                    .setAction("UPGRADE NOW", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                           Intent regi = new Intent(IndexPage.this,RegistrationActivity.class);
                            startActivity(regi);
                        }
                    }).show();

        }else {
            Intent login = new Intent(this, LoginCyclos.class);
            startActivity(login);
        }*/
    }
}
