package com.mobicash.mobivisa;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;


public class ViewImageFull extends Activity {
	ImageView imageView;
	Bitmap photo;
	byte[] byteArray;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.imageview_layout);
		Bundle extr = getIntent().getExtras();
		byteArray = extr.getByteArray("image");
		photo = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
		imageView = (ImageView) findViewById(R.id.bigimage);
		imageView.setImageBitmap(photo);
		
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.viewimagefull, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()){
			case R.id.done:
				onBackPressed();
				break;
			
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	

}
