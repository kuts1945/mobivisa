package utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;


import com.mobicash.mobivisa.MainActivity;
import com.mobicash.mobivisa.R;

import dbstuff.AndChat;
import dbstuff.MessageDb;


public class Alerter {
	
	//Shared prefs
	SharedPreferences prefs;
	Editor edit;
	Vars vars;
	TextView serverText;
	AlertDialog levelDialog;

	Context context;

	public Alerter(Context context) {
		this.context = context;
		
		
	}
	
	//----------------------------------------------------------------------------
	// ALERT GENERATOR
	//public void alerter(String title, String message,Context context) {
	public void alerter(String title, String message, Context context) {


		// public void onClick(View arg0) {
		// loadEm();
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

		// set title
		alertDialogBuilder.setTitle(title);

		// set dialog message
		alertDialogBuilder.setMessage(message).setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, close
						// current activity
						// MainActivity.this.finish();


						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
		// }
	}


	
	public void alerterSuccess(String header, String message){
		
		final String header2 = header;
		
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView;
		 promptsView = li.inflate(R.layout.dialog_success_simple, null);
		 
		 TextView headerTxt = (TextView) promptsView.findViewById(R.id.success_simple_header);
		 TextView messageTxt = (TextView) promptsView.findViewById(R.id.success_simple_message);

		 headerTxt.setText(header);
		 messageTxt.setText(message);

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					context);

			// set prompts.xml to alertdialog builder
			alertDialogBuilder.setView(promptsView);
			// set dialog message
			alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								//dialog.cancel();
								dialog.cancel();

								//CHECK IF ITS A PAYMENT BEING MAKE
								if (header2.equalsIgnoreCase("Success")) {
									Intent i = new Intent();
									i.setClass(context, MainActivity.class);
									context.startActivity(i);
								}

							}
						})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
	}
	public void alerterSuccessSimple(String header, String message){
		
		final String header2 = header;
		
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView;
		 promptsView = li.inflate(R.layout.dialog_success_simple, null);
		 
		 TextView headerTxt = (TextView) promptsView.findViewById(R.id.success_simple_header);
		 TextView messageTxt = (TextView) promptsView.findViewById(R.id.success_simple_message);

		 headerTxt.setText(header);
		 messageTxt.setText(message);

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					context);

			// set prompts.xml to alertdialog builder
			alertDialogBuilder.setView(promptsView);
			// set dialog message
			alertDialogBuilder
				.setCancelable(false)
				
				.setNegativeButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
	}
public void alertercheck(String header, final String message, final String packagename ,final String recep,final String otherName,final String status,final long amount ,final String recepNumber){
	vars= new Vars(context);	

	final String header2 = header;
	
	LayoutInflater li = LayoutInflater.from(context);
	View promptsView;
	 promptsView = li.inflate(R.layout.dialog_success_simple, null);
	 
	 TextView headerTxt = (TextView) promptsView.findViewById(R.id.success_simple_header);
	 TextView messageTxt = (TextView) promptsView.findViewById(R.id.success_simple_message);

	 headerTxt.setText(header);
	 messageTxt.setText(message);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder
			.setCancelable(false)
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						})
			.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							//dialog.cancel();
							//dialog.cancel();


							AndChat ande = new AndChat();
							String threadid = (vars.username + "X" + recep);
							//	ande.message = "Please register";
							ande.setDelRecep("no");
							ande.setDelServ("no");
							if (status.equalsIgnoreCase("success")) {
								ande.setMessage("You have transfered " + amount + " to " + otherName);
								ande.setMessageId("99");
							} else if (status.equalsIgnoreCase("successAir")) {
								ande.setMessage("You have credited " + amount + " to " + otherName);
								ande.setMessageId("10");
							} else {

								ande.setMessage("Gift worth " + amount + " sent to" + otherName);
								ande.setMessageId("19");

								//ande.setMessage("I want to send you money please upgrade your registration");
							}
							if (status.equalsIgnoreCase("success") || status.equalsIgnoreCase("successAir")) {
								ande.setThreadid("money");
							} else {
								ande.setThreadid(status);

							}
							ande.setRecep(recep);
							ande.setUserId(vars.chk);
							ande.setMessageType("chat");
							ande.setUserName(vars.username);

							ande.setTimeRec(amount);
							//ande.setName(userName);
							ande.setOtherName(otherName);
							ande.setTimeSent(System.currentTimeMillis());
							ande.setSender(vars.chk);
							ande.setLanguage(vars.language);
							ande.setMsgID(1L);
							if (vars.recepMobile == null) {
								ande.setRecepNumber(recepNumber);
							} else {
								ande.setRecepNumber(vars.recepMobile);
							}
							ande.setMobile(vars.mobile);
							//	log ("Our threadId is :==="+threadid);

							log("Alert recep is :===" + ande.getRecep() + "TIME REC *****" + ande.getTimeRec());
							log("Alert RECEPNUMBER :===" + ande.getRecepNumber());
							log("Alert othername :===" + ande.getOtherName());
							log("Alert GETMOBILE is :===" + ande.getMobile());

							log("Alert username is :===" + ande.getUserName().toString() + " userid==" + ande.getUserId().toString());

							MessageDb sdb = new MessageDb(vars.context, ande.getThreadid().toString(), ande.getTimeRec(), ande.getMessage().toString(), ande.getMessageType().toString(),
									ande.getRecep().toString(), ande.getMessageId(), ande.getOtherName().toString(), ande.getSender().toString(), ande.getTimeSent(), ande.getUserName().toString(), ande.getUserId().toString(),
									ande.getDelServ().toString(), ande.getDelRecep().toString(), ande.getMobile().toString(), ande.getRecepNumber().toString(), ande.getMsgID(), ande.getLanguage(), true);
							sdb.save();
							log("Alter*****MSG ID IS================" + ande.getMessageId());
							vars.msgid = sdb.getId();
							//MqUtils.sendMessage(context, ande, vars.msgid);

							Intent i = new Intent(packagename);
							i.putExtra("recep", recep);
							i.putExtra("otherName", otherName);
							i.putExtra("recepNumber", recepNumber);
							i.putExtra("laguage", vars.language);
							context.startActivity(i);


//			    	}
//			    
						}
					});
		

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}
	public String log (String string){
		System.out.println(string);
		return string;
		
	}
	
	
/*	public void alerter_suc_umeme(String bal, String amount,String account,String name){
		
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView;
		 promptsView = li.inflate(R.layout.alert_umeme, null);
		
		 TextView text_name = (TextView) promptsView.findViewById(R.id.text_name);
		 TextView text_account = (TextView) promptsView.findViewById(R.id.text_account);
		 TextView text_bal = (TextView) promptsView.findViewById(R.id.text_bal);
		 TextView text_amount = (TextView) promptsView.findViewById(R.id.text_amount);

		 text_name.setText(name);
		 text_account.setText(account);
		 text_bal.setText(bal);
		 text_amount.setText(amount);

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					context);

			// set prompts.xml to alertdialog builder
			alertDialogBuilder.setView(promptsView);
			// set dialog message
			alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton("Cancel",
				  new DialogInterface.OnClickListener() {
				    public void onClick(DialogInterface dialog,int id) {
				    	//dialog.cancel();
				    	dialog.cancel();
				    	
				    
				    }
				  })
				.setNegativeButton("Pay",
				  new DialogInterface.OnClickListener() {
				    public void onClick(DialogInterface dialog,int id) {
					
				    }
				  });

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
	}*/
	public void login(String header, String message){

		final String header2 = header;

		LayoutInflater li = LayoutInflater.from(context);
		View promptsView;
		promptsView = li.inflate(R.layout.dialog_success_simple, null);

		TextView headerTxt = (TextView) promptsView.findViewById(R.id.success_simple_header);
		TextView messageTxt = (TextView) promptsView.findViewById(R.id.success_simple_message);

		headerTxt.setText(header);
		messageTxt.setText(message);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton("Login",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {

							//	dialog.cancel();

								//CHECK IF ITS A PAYMENT BEING MAKE
							//	if(header2.equalsIgnoreCase("Success")){
									Intent i = new Intent();
									i.setClass(context, MainActivity.class);
									context.startActivity(i);
							//	}

							}
						})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {

								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}
	
	

}
