package utils;

import com.google.gson.Gson;


import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class Vars {

	// JSON OBJECT TO HOLD REPLY
	
	// APP VARS
	public String username;
    public String server_sa;
   public String server_rw;
	public String electricity_number;
	public String smartcard;
	public String pin;
	public String server;
	public String mqttser;
	public String fav_blurbs;
	public String onof;
	public String userId;
	public Long msgid;
	public String recep;
	public String cyclose;
	public String active;
	public String chk;
	public String mobile;
	public String country_code;
	public String font;
	public String recepMobile;
	public String location;
	public String language;
	public String sender;
	public String wallpaper;
	public SharedPreferences prefs ,shprefs;
	public Editor edit;
	public String cyclos_number;
   public String mylife;
	public ProgressDialog pd;
	public String imei;
	public Gson gson;
	public String fullname;
	public String profile_url;
	public Context context;
	private Boolean logg= true;
	
	public Vars(Context context) {
		// this.alerter = new Alerter(context);
		prefs = PreferenceManager.getDefaultSharedPreferences(context);
		server = prefs.getString("server", "http://54.68.162.113/sugaapp/");
        server_sa = prefs.getString("server", "http://test.mobicash.co.za/bio-api/mobiSquid/");
      server_rw = prefs.getString("server","http://test.mcash.rw/bio-api/mobiSquid/");
      font = prefs.getString("font", null);
		profile_url= prefs.getString("profile_url", null);
		wallpaper = prefs.getString("wallpaper", null);
		cyclos_number = prefs.getString("cyclos_number",null);
		electricity_number = prefs.getString("electricity_number", null);
		smartcard = prefs.getString("smartcard", null);
      mylife = prefs.getString("mylife",null);
		active = prefs.getString("active",null);
		location = prefs.getString("location", null);
		mqttser = prefs.getString("mqttser", "162.248.4.200:1883");
		cyclose = prefs.getString("cyclose", null);
		fullname = prefs.getString("fullname",null);
		sender = null;
		msgid = null;
		onof= null;
		// TEMP REMOVE LATER
	  //userName = "andyTest";
		this.context=context;

		// TEMP REMOVE LATER
		// userName = "mworoziIDEOS";
		//username = "andrewConn";
		chk = prefs.getString("userId", null);
		recep = prefs.getString("recep",null);
		pin = prefs.getString("pin",null);
		username = prefs.getString("username", null);
		language = prefs.getString("language", null);
		fav_blurbs= prefs.getString("fav_blurbs", null);
		mobile = prefs.getString("mobile", null);
		country_code = prefs.getString("country_code", null);
		imei = prefs.getString("imei", null);
		recepMobile = prefs.getString("recepNumber", null);
		//username = "andrew";
		edit = prefs.edit();
		// ALERTER
		
		
		gson = new Gson();
		
	}
	public void log(String string){
		if (logg){
		System.out.println(string);	
		}else{
			
		}
	}
	public void hideKeyboard(View view,Context context)
	{
		InputMethodManager in = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}

}
