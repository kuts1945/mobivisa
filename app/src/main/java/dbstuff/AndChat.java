package dbstuff;

import android.provider.Settings.Global;

public class AndChat {
   public Boolean getIs_self() {
      return is_self;
   }

   public void setIs_self(Boolean is_self) {
      this.is_self = is_self;
   }

   private Boolean is_self;
	
	private String message;
	private String messageType;
	private String recep;
	private String messageId;
	private byte[] bytecontent;
	private String language;
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public byte[] getBytecontent() {
		return bytecontent;
	}

	public void setBytecontent(byte[] byteArray) {
		this.bytecontent = byteArray;
	}
	private String sender;
	private long timeSent;
	private long timeRec;
	private String threadid;
	private String delServ;
	private String delRecep;
	private String userName;
	private String otherName;
	private String userId;
	private String mobile;
	private String recepNumber;
	private Long msgID;

	public Long getMsgID() {
		return msgID;
	}

	public void setMsgID(Long msgID) {
		this.msgID = msgID;
	}

	public String getRecepNumber() {
		return recepNumber;
	}

	public void setRecepNumber(String recepNumber) {
		this.recepNumber = recepNumber;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOtherName() {
		return otherName;
	}

	public void setOtherName(String otherName) {
		this.otherName = otherName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getDelRecep() {
		return delRecep;
	}

	public void setDelRecep(String delRecep) {
		this.delRecep = delRecep;
	}
	
	public String getDelServ() {
		return delServ;
	}

	public void setDelServ(String delServ) {
		this.delServ = delServ;
	}

	public String getThreadid() {
		return threadid;
	}

	public void setThreadid(String threadid) {
		this.threadid = threadid;
	}
	public long getTimeRec() {
		return timeRec;
	}

	public void setTimeRec(long timeRec) {
		this.timeRec = timeRec;
	}
	public long getTimeSent() {
		return timeSent;
	}

	public void setTimeSent(long timeSent) {
		this.timeSent = timeSent;
	}
	
	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}
	
	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public String getRecep() {
		return recep;
	}

	public void setRecep(String recep) {
		this.recep = recep;
	}
	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	

}
